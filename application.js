/* eslint-env browser */

/**
 * Implements code for integrating common features with a consuming theme at
 * site's runtime.
 */

import Components from "./components/index.js";
import { setup as fontAwesomeSetup } from "./features/fontawesome.js";
import VptcNotFound from "./components/VptcNotFound.vue";

import paths from "./data/paths.json";
import redirections from "./data/redirections.json";
import { isServer } from "./lib/utilities.js";

export { default as VptcNotFound } from "./components/VptcNotFound.vue";

/**
 * Sets up integration of this common library with a particular theme's
 * implementation.
 *
 * @param {EnhanceAppContext} context exposes runtime context of client-side application
 * @returns {void}
 */
export function enhanceApp( context ) {
	const { app, siteData: { value: { base, headTags, themeConfig: { fontAwesome } } } } = context;

	fixHeadTags();

	Components.forEach( component => {
		app.component( component.name, component );
	} );

	if ( fontAwesome ) {
		fontAwesomeSetup( context, fontAwesome );
	}


	if ( !isServer ) {
		const url = new URL( location.href );
		let pathname = url.pathname;

		if ( pathname.startsWith( base ) ) {
			pathname = "/" + pathname.slice( base.length );
		}

		let redirect = pathname;

		if ( !paths[pathname] ) {
			const candidate = pathname + "/";

			if ( paths[candidate] ) {
				redirect = candidate;
			}
		}

		redirect = redirections[redirect] ?? redirect;

		if ( redirect && redirect !== pathname ) {
			url.pathname = base + redirect.slice( 1 );
			// FIXME this approach alone does not prevent double history entry on entering a site
			history.replaceState( {}, "", String( url ) );
		}
	}


	/**
	 * Validates configured set of meta tags adding some required tag if missing.
	 *
	 * @returns {void}
	 */
	function fixHeadTags() {
		const tags = headTags ?? [];

		const found = tags.find( ( [ tag, { name } ] ) => tag.trim().toLowerCase() === "meta" && name === "format-detection" );
		if ( found == null ) {
			tags.push( [ "meta", { name: "format-detection", content: "telephone=no" } ] );
		}
	}
}

/**
 * Adds custom setup code to the resulting site's application runtime.
 *
 * @returns {void}
 */
export function setup() {
	// TODO add custom code here if necessary
}

/**
 * Conveniently injects this library in browser runtime for creating a new
 * VitePress theme.
 *
 * @param {Component} layout Vue component rendering pages' layout
 * @returns {Theme} description of theme for use theme's index.js
 */
export function createThemeWithLayout( layout ) {
	return {
		Layout: layout,
		NotFound: VptcNotFound,
		enhanceApp: ( ...args ) => enhanceApp( ...args ),
		setup: ( ...args ) => setup( ...args ),
	};
}
