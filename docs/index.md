---
layout: home

hero:
  name: VPTC VitePress Theme Commons
  tagline: A collection of common features and tools for implementing websites with VitePress.
  actions:
    - theme: brand
      text: Get started!
      link: getting-started.html 
    - theme: alt
      text: Features
      link: features/index.html
---
