# Search database

All terms in every content file of a site are collected and exposed in a series of JSON files mostly for implementing a client-side search engine.

## Terms file

Every term found in any of the site's pages is normalized and then collected in a single JSON file which is exposing a single string listing all those normalized terms separated by single whitespace.

Any sequence of at least three digits or letters are considered terms if it starts with a letter. This does not apply to HTML tags and their attributes' values. 

Normalization is about converting all letters of found terms to lowercase.

The file can be fetched as **/search/index.json**. It describes an object containing a single property **keywords** which is that string described above.

## Reference files

For every term, another JSON file is created. Every such file provides pages containing the term and either page's number of a term's occurrences.

The files can be fetched as **/search/&lt;term>.json** with **&lt;term>** being replaced with either normalized term. Either file contains an object mapping the numeric index of a page containing either term into the number of occurrence on that page. That numeric index of a page can be used with the [sitemap](sitemap.md) data to eventually get either page's pathname and its front matter.

## How to search

Given a user's query, a fulltext search will start searching for those queries in the terms file. This way, matches can be mapped into the terms containing them.

Given that list of containing terms, either term's reference file can be fetched alongside the sitemap data to get the matching documents of the site. Given the occurrences per term and page, a weight may be applied to either match.
