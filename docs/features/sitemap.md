# Sitemap

All pages of a site are collected and exposed as a list of page descriptions in a JSON file re-created on every run of VitePress. This file is then used e.g. to create navigation controls.

Every page's descriptor consists of

* property **path** providing local pathname of related page,
* property **lastUpdated** providing the source file's time of latest update given in seconds since Unix Epoch and
* property **frontmatter** which is an object containing any fronnt matter defined in the source file.

The sorting of listed pages is essential e.g. when processing search data which is selecting pages by numeric index for reduced redundancy and file sizes.
