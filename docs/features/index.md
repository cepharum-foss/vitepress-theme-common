# Features

This library includes several features commonly useful for implementing a rather regular website with VitePress.

* [Sitemap](sitemap.md)
* [Search database](search.md)
* Translations management
* Internationalization
* Sections
* Navigation controls
* Terms popup
* FontAwesome icons
* Redirections

