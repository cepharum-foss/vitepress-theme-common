# Getting started

## Add commons to your theme

### Install dependency

Let's assume you have a project for generating a static site with VitePress. Now you want to customize the site's theme, and it should be based on features provided by this library.

Install this library as a dependency in your project by running npm in its folder:

```bash
npm install -D @cepharum/vitepress-theme-common
```

### Inject library on server side

When developing the site, VitePress is providing the files of your site's pages using a local web server. A similar process is used when building your static site for deployment. For sake of simplicity, this tutorial is considering both contexts as _server-side_ and you must inject VitePress common library there. This is feasible via [your site's configuration](https://vitepress.vuejs.org/guide/configuration), only, which is defined in file **.vitepress/config.js** of your project.

Let's assume that file looks like this:

```javascript
export default {
  title: "VitePress",
  description: "Just playing around."
};
```

For injecting VitePress common library, you have to adjust that file:

```javascript
import { enhanceConfig } from "@cepharum/vitepress-theme-common/config.js";

export default enhanceConfig( {
  title: "VitePress",
  description: "Just playing around."
} );
```

As illustrated, a helper function of VitePress common library must be invoked on your site's actual configuration to inject the library.

:::warning Mind the import source
Importing from library using single entry point does not work for the library must provide separate code for two different runtime environments. Thus, the import is particularly addressing **config.js** of VitePress common library.
:::

### Inject library on client side

Sites based on VitePress are rich application running in a user's browser. Some setup is required there, too. You need to create a custom theme for injecting the library's code as part of it.

A custom theme specific to your site [is added in folder .vitepress/theme of your project](https://vitepress.vuejs.org/guide/theme-introduction#using-a-custom-theme). Create a file named **index.js** in that folder with content like this:

```javascript
import { VptcNotFound, enhanceApp as enhanceAppCommon } from "@cepharum/vitepress-theme-common/application.js";

import Layout from "./layout.vue";
import "./style.scss";

export default {
	Layout,

	NotFound: VptcNotFound,

	enhanceApp(ctx) {
		enhanceAppCommon(ctx);
	}
}
```

This example is defining

* a template laying out common elements of your site's pages,
* a component to use if a requested page hasn't been found and
* a function invoked in browser when loading the site's application

to be picked up by VitePress when running in a browser. The function invoked in last step is essential here, and it is basically the browser-side counterpart to the helper function that has been invoked on server-side for qualifying your site's configuration.

:::warning Mind the import source
Importing from library using single entry point does not work for the library must provide separate code for two different runtime environments. Thus, the import is particularly addressing **application.js** of VitePress common library.
:::

### Layout

In previous step, a custom layout is imported and exposed. You need to create it as a Vue component in file **.vitepress/theme/layout.vue**. The most simple example would be:

```vue
<template>
	<Content/>
</template>
```

The layout can be any kind of Vue component. The special [&lt;Content> component](https://vitepress.vuejs.org/guide/api#content) is marking the spot where all your pages' content is injected.

A slightly more elaborate example could look like this:

```vue
<template>
	<div class="site">
		<header class="header">
			<h1>My Website</h1>
		</header>
		<main class="main">
			<Content/>
		</main>
		<footer class="footer">
			&copy; {{ thisYear }} My Company
		</footer>
	</div>
</template>

<script>
export default {
	data: () => ( {
		thisYear: new Date().getFullYear(),
	} ),
}
</script>
```

### Page not found

VitePress is injecting the `NotFound` component provided in your site's definition file above whenever the user is navigating to a page that does not exist.

This library's `VptcNotFound` component is including a configurable redirection which is useful for SEO e.g. on converting or refactoring a site.

## Extended content

Following those steps above, your site is now set up to use extended features of this library. E.g. you can have custom sections now.

Create or update file **index.md** of your project:

```markdown
# Hello world!

This is a regular paragraph.

:::section custom
And this is some content that's additionally wrapped in a container suitable 
for generating visual building blocks such as promoting content in a box.

This second paragraph is part of that container, too.
:::
```

This source results in a rendered document similar to this:

```html
<h1 id="hello-world" tabindex="-1">
	Hello world!
	<a class="link local asset header-anchor" href="/#hello-world" title="">#</a>
</h1>
<p>This is a regular paragraph.</p>
<section class="section type-custom without-image without-icon with-content">
	<div>
		<p>
			And this is some content that's additionally wrapped in a container 
			suitable for generating visual building blocks such as promoting 
			content in a box.
		</p>
		<p>
			This second paragraph is part of that container, too.
		</p>
	</div>
</section>
```

Such sections support attributes and options. E.g., that extra `custom` written after `:::section` in the source file is resulting in a class `type-custom` applied to the resulting HTML e.g. for custom styling.

There are more predefined kinds of containers like _sections_ illustrated here. For example, you can create a site of tiles in a proper container like this, too:

```markdown
# Hello world!

There are benefits:

::::tiles
:::tile
Performance
:::

:::tile
Independence
:::

:::tile
Reliance
:::
::::
```

This example is nesting containers of type `tile` inside a container of type `tiles`.

:::warning Pay attention to the colons!
When nesting containers, a different number of colons must be used to start and end a container. 3 colons must be used at least.
:::

Such containers mostly result in a different HTML rendered. It is up to your theme's styling definitions to control how either container looks like.

## Extended front matter

This library is processing certain meta information that might be included with pages' front matter. This includes the declaration of a page's language and references to pages that contain its translations.

```markdown
---
lang: en
de: 
---

# Hello!
```
