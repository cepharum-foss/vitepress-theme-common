export default {
	title: "VitePress Theme Commons",
	description: "A collection of common features and tools for implementing websites with VitePress.",
	outDir: "../public",
	base: "/vitepress-theme-common/",
	themeConfig: {
		nav: [
			{
				text: "Getting started",
				link: "/getting-started.html"
			},
			{
				text: "Features",
				items: [
					{ text: "Search", link: "/features/search.html" },
					{ text: "Sitemap", link: "/features/sitemap.html" },
				]
			}
		],
		footer: {
			message: "Released under the MIT License.",
			copyright: "Copyright © 2022-present cepharum GmbH, Berlin"
		}
	}
}
