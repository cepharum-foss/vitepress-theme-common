import VptcReference from "./basics/VptcReference.vue";
import VptcLinkList from "./navigation/VptcLinkList.vue";
// import VptcNavigator from "./navigation/VptcNavigator.vue";
// import VptcSitemap from "./navigation/VptcSitemap.vue";
// import VptcInjectedContent from "./VptcInjectedContent.vue";
// import VptcSiteLogo from "./VptcSiteLogo.vue";
// import VptcDrawer from "./VptcDrawer.vue";
// import VptcTiles from "./container/VptcTiles.vue";
// import VptcTile from "./container/VptcTile.vue";
import VptcSection from "./container/VptcSection.vue";
// import VptcBox from "./container/VptcBox.vue";
// import VptcAccordion from "./container/VptcAccordion.vue";
import VptcImage from "./basics/VptcImage.vue";
// import VptcTranslations from "./navigation/VptcTranslations.vue";
// import VptcTermsPopup from "./VptcTermsPopup.vue";
import VptcNotFound from "./VptcNotFound.vue";
import VptcFa from "./VptcFa.vue";

export default [
	VptcReference,
	VptcLinkList,
	// VptcNavigator,
	// VptcSitemap,
	// VptcInjectedContent,
	// VptcSiteLogo,
	// VptcDrawer,
	// VptcTiles,
	// VptcTile,
	VptcSection,
	// VptcBox,
	// VptcAccordion,
	VptcImage,
	// VptcTranslations,
	// VptcTermsPopup,
	VptcNotFound,
	VptcFa,
];
