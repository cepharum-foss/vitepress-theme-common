/**
 * Implements code for integrating common features with build process of a
 * consuming theme.
 */

import Container from "markdown-it-container";

import sitedataPlugin from "./plugins/sitedata.js";
import l10nCompilerPlugin from "./plugins/l10n-compiler.js";

/**
 * Maps functional HTML characters into related character entity literally
 * representing that character.
 *
 * @type {Object<string, string>}
 */
const HtmlCodes = {
	'"': "&quot;",
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
};

/**
 * Matches all functional HTML characters.
 *
 * @type {RegExp}
 */
const ptnHtmlUnsafeCharacters = new RegExp( "[" + Object.keys( HtmlCodes ).join( "" ) + "]", "g" );

/**
 * Extends provided configuration of a site primarily to inject support for
 * additional Markdown syntax.
 *
 * @param {UserConfig} config exposes current site's configuration to be extended
 * @returns {UserConfig} extended configuration
 */
export function enhanceConfig( config ) {
	return {
		...extendMarkdown( config ),
		vite: {
			...config?.vite,
			plugins: [
				...config?.vite?.plugins || [],
				sitedataPlugin,
				l10nCompilerPlugin,
			],
		},
	};
}


/**
 * Encodes functional HTML characters using character entities.
 *
 * @param {string} raw string potentially containing functional HTML characters
 * @return {string} provided string with any functional HTML character disabled
 */
export function escapeHtml( raw ) {
	return raw.replace( ptnHtmlUnsafeCharacters, code => HtmlCodes[code] );
}

/**
 * Compiles string describing HTML attribute for use in an opening HTML tag.
 *
 * @param {string} info string following sequence of colons opening container
 * @returns {string} HTML-encoded attributes extracted from provided string
 */
export function mdRenderHtmlAttributes( info ) {
	const parameters = info.replace( /^\s*[a-z-]+\s*/i, "" );
	let args = "";

	if ( parameters.length > 0 ) {
		const ptn = /\s*(--)?([a-z]+(?:[-_]{1,2}[a-z0-9]+)*)([=:]([^\s"']\S*|(["']).*?\5))?/gi;
		const switches = {};
		let hasSwitches = false;
		let sub;

		while ( ( sub = ptn.exec( parameters ) ) != null ) {
			let key = sub[2].toLowerCase();

			if ( !sub[1] ) {
				key = key.replace( /-([a-z])/g, ( _, initial ) => initial.toUpperCase() );
			}

			if ( sub[3] ) {
				const value = sub[4].replace( /^(["'])(.*)\1$/, "$2" );

				args += ` ${key}="${escapeHtml( value )}"`;
			} else {
				hasSwitches = switches[key] = true;
			}
		}

		if ( hasSwitches ) {
			args += ` :switches="${escapeHtml( JSON.stringify( switches ) )}"`;
		}
	}

	return args;
}

/**
 * Renders markdown-it AST tokens of containers as HTML.
 *
 * @param {string} name name of token
 * @param {object[]} tokens whole list of tokens
 * @param {int} idx index of token to process into list of tokens
 * @return {string} HTML code representing selected token
 */
export function mdRenderContainer( name, tokens, idx ) {
	const token = tokens[idx];

	if ( token.nesting === 1 ) {
		return `<${name}${mdRenderHtmlAttributes( token.info )}>\n`;
	}

	return `</${name}>\n`;
}

/**
 * Extends provided configuration to support additional markdown syntax.
 *
 * @param {UserConfig} config configuration to extend
 * @returns {object} provided configuration
 */
function extendMarkdown( config ) {
	const existingMdConfiguration = config?.markdown?.config;

	config.markdown ??= {};             // eslint-disable-line no-param-reassign
	config.markdown.config = md => {    // eslint-disable-line no-param-reassign
		if ( existingMdConfiguration ) {
			existingMdConfiguration( md );
		}

		md.use( Container, "section", { render: ( tokens, idx ) => mdRenderContainer( "VptcSection", tokens, idx ) } );
		md.use( Container, "tiles", { render: ( tokens, idx ) => mdRenderContainer( "VptcTiles", tokens, idx ) } );
		md.use( Container, "tile", { render: ( tokens, idx ) => mdRenderContainer( "VptcTile", tokens, idx ) } );
		md.use( Container, "accordion", { render: ( tokens, idx ) => mdRenderContainer( "VptcAccordion", tokens, idx ) } );
		md.use( Container, "box", { render: ( tokens, idx ) => mdRenderContainer( "VptcBox", tokens, idx ) } );

		// use <Reference> for rendering links instead of <a>
		// eslint-disable-next-line no-param-reassign,camelcase
		md.renderer.rules.link_open = function( tokens, idx ) {
			const token = tokens[idx];
			const attrs = [];

			const classes = token.attrGet( "class" );
			const href = token.attrGet( "href" );

			if ( classes ) {
				attrs.push( ` class="${escapeHtml( classes )}"` );
			}

			if ( href ) {
				attrs.push( ` href="${escapeHtml( href )}"` );
			}

			return `<VptcReference${attrs.join( "" )}>`;
		};

		// eslint-disable-next-line no-param-reassign,camelcase
		md.renderer.rules.link_close = function() {
			return `</VptcReference>`;
		};

		// use <VptcImage> for rendering images instead of <img>
		// eslint-disable-next-line no-param-reassign
		md.renderer.rules.image = function( tokens, idx ) {
			const token = tokens[idx];

			let src = token.attrGet( "src" );
			if ( !src ) {
				return "";
			}

			const extra = {
				type: undefined,
				width: undefined,
				height: undefined
			};
			const match = /#([^#]+)$/.exec( src );

			if ( match ) {
				const details = /^((?:[a-z_-][a-z0-9_-]*)(?:\/(?:[a-z_-][a-z0-9_-]*))*)?(?:@?([1-9]\d*)?x([1-9]\d*)?)?$/i.exec( match[1] );
				if ( !details ) {
					throw new Error( `invalid styling options in image URL ${src}` );
				}

				src = src.slice( 0, match.index );

				extra.type = details[1] == null ? undefined : `'${details[1]}'`;
				extra.width = parseInt( details[2] ) || undefined;
				extra.height = parseInt( details[3] ) || undefined;
			}

			const alt = escapeHtml( token.attrGet( "alt" ) || "" );
			const url = escapeHtml( src );

			return `<VptcImage url="${url}" alt="${alt}" :type="${extra.type}" :width="${extra.width}" :height="${extra.height}"></VptcImage>`;
		};
	};

	return config;
}
