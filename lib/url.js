export const ptnHasScheme = /^\w+:/;
export const ptnHasCommonScheme = /^(?:https?|ftps?|data):/i;
export const ptnIsNonRelative = /^(?:\/|\w+:|#|\?)/;
export const ptnIsLocal = /^(?!\w+:)/;
export const ptnIsPage = /(?:^|\.(?:md|html)?)(?:[?#]|$)/i;
export const ptnIsLocalAnchor = /^#/;
export const ptnQueryAndHash = /[#?].*$/;
export const ptnLastSegment = /\/[^/]*$/;
export const ptnSplitQueryAndHash = /^([^#?]*)([#?].*)?$/;
export const ptnMarkdownFilename = /\.md($|[?#])/;

/**
 * Resolves relative path in context of provided base path with the latter
 * assumed to address a file (not its folder).
 *
 * @param {string} base path name of URL second path name is relative to
 * @param {string} relative path name considered relative to first pathname
 * @returns {string} absolute path name of URL equivalent to relative one
 */
export function resolve( base, relative ) {
	if ( ptnIsNonRelative.test( relative ) ) {
		return relative;
	}

	if ( !relative ) {
		return base;
	}

	let resolved = base.replace( ptnLastSegment, "" );

	if ( base.indexOf( "/" ) < 0 ) {
		resolved = "";
	}

	for ( const segment of relative.split( "/" ) ) {
		switch ( segment ) {
			case "" :
			case "." :
				break;

			case ".." :
				if ( resolved.indexOf( "/" ) > -1 ) {
					resolved = resolved.replace( ptnLastSegment, "" );
				} else {
					resolved = "";
				}
				break;

			default :
				resolved += "/" + segment;
		}
	}

	return resolved || "/";
}

/**
 * Replaces redirections targeting another redirected page with their final
 * target.
 *
 * @param {Object<string,string>} redirections maps page's relative path names into either one's redirection target
 * @returns {Object<string,string>} map of redirections with all targets selecting a non-redirecting page
 */
export function optimizeRedirections( redirections ) {
	const resolver = ( context, url ) => {
		const [ , path = "", queryAndHash = "" ] = ptnSplitQueryAndHash.exec( url );
		const resolved = resolve( context, path );

		if ( redirections.hasOwnProperty( resolved ) ) {
			const target = resolver( resolved, redirections[resolved] );
			const [ , targetPath = "", targetQueryAndHash = "" ] = ptnSplitQueryAndHash.exec( target );

			redirections[url] = targetQueryAndHash ? target : targetPath + queryAndHash; // eslint-disable-line no-param-reassign

			return redirections[url];
		}

		return resolved + queryAndHash;
	};

	for ( const source of Object.keys( redirections ) ) {
		redirections[source] = resolver( source, redirections[source] ); // eslint-disable-line no-param-reassign
	}

	return redirections;
}
