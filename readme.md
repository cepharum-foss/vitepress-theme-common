# VitePress theme commons

A collection of common features and tools for implementing custom themes for [Vitepress](https://vitepress.vuejs.org).

## License

[MIT](license)

## Manual

[online documentation](https://cepharum-foss.gitlab.io/vitepress-theme-common)
