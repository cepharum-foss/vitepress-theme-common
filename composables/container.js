import { computed } from "vue";
import { useAsset } from "./asset.js";

/**
 * Generates properties for composing components resembling containers.
 *
 * @param {any} props reactive set of component properties
 * @param {any} context provides application instance, slots and more
 * @returns {object} composable properties of containers
 */
export function useContainer( props, context ) {
	const image = useAsset( "image", props, context );

	const containerClassNames = computed( () => {
		const classes = Object.keys( props.switches || {} )
			.filter( name => !/^fa(?:-|Transform|Mask|Animate)/.test( name ) )
			.map( name => `type-${name}` );

		if ( props.image ) {
			classes.push( "with-image" );
			classes.push( image.ready.value ? "image-loaded" : "image-loading" );
		} else {
			classes.push( "without-image" );
		}

		classes.push( props.icon ? "with-icon" : "without-icon" );
		classes.push( context.slots.default ? "with-content" : "without-content" );

		if ( props.color || props.text ) {
			classes.push( "custom-color" );
		}

		return classes;
	} );

	const iconClassNames = computed( () => {
		return Object.keys( props.switches || {} )
			.filter( name => /^fa-(?!fixed-width$)/.test( name ) )
			.map( name => name.replace( /^fa-/, "" ) );
	} );

	const iconsFixedWidth = computed( () => Boolean( props.switches.faFixedWidth || props.switches["fa-fixed-width"] ) );

	const containerStyle = computed( () => {
		let assigned = false;
		const result = {};

		if ( props.color ) {
			assigned = true;
			result.backgroundColor = props.color;
		}

		if ( props.text ) {
			assigned = true;
			result.color = props.text;
		}

		return assigned ? result : undefined;
	} );

	return {
		image,

		containerClassNames,
		iconClassNames,
		iconsFixedWidth,
		containerStyle,
	};
}

/**
 * Defines properties for components resembling containers.
 *
 * @type {object}
 */
export const props = {
	color: {
		type: String,
		validator: value => value == null || /^#?[0-9a-z]{3}(?:[0-9a-z]{3}(?:[0-9a-z]{2})?)?$/i.test( value ),
		note: "Selects custom background color to apply on container. " +
		      "Color is provided in hex format with or without leading " +
		      "hash, e.g. `#66aa66`.",
	},
	text: {
		type: String,
		validator: value => value == null || /^#?[0-9a-z]{3}(?:[0-9a-z]{3}(?:[0-9a-z]{2})?)?$/i.test( value ),
		note: "Selects custom text color to apply on current container. " +
		      "Color is provided in hex format with or without leading " +
		      "hash, e.g. `#66aa66`.",
	},
	switches: {
		type: Object,
		default: () => ( {} ),
		note: "Maps names of boolean switches into truthy information " +
		      "whether either switch is enabled or not. Switches aren't " +
		      "controlling anything particular, but will be listed as " +
		      "classes of container to probably affect its styling via CSS.",
	},
	image: {
		type: String,
		default: "",
		note: "Selects image by its URL for showing in context of container.",
	},
	icon: {
		type: [ String, Array ],
		default: "",
		note: "Names icon to show on section."
	},
	faTransform: {
		type: String,
		default: "",
		note: "Selects one or more power transformations to apply on icon " +
		      "as supported by fortawesome."
	},
	faMask: {
		type: String,
		default: "",
		note: "Selects icon provided shape for masking actual icon of " +
		      "container."
	},
	faAnimate: {
		type: String,
		default: "",
		note: "Selects animation for icon as supported by fortawesome."
	},
};
