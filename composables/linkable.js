import { computed } from "vue";
import { useData, useRoute, useRouter } from "vitepress";
import { ptnAssetPrefix } from "../lib/utilities.js";
import { ptnIsIndex, useSitemap } from "./sitemap.js";
import { useEvent } from "./event.js";
import {
	ptnHasCommonScheme,
	ptnHasScheme, ptnIsLocalAnchor,
	ptnIsNonRelative,
	ptnIsPage, ptnMarkdownFilename,
	ptnSplitQueryAndHash, resolve
} from "../lib/url.js";

/**
 * Creates composable for generating linkable component.
 *
 * @param {object} props reactive properties of a component
 * @returns {object} properties representing a linkable component
 */
export function useLinkable( props ) {
	const route = useRoute();
	const router = useRouter();
	const globalEvent = useEvent();
	const sitemap = useSitemap();
	const data = useData();

	const redirections = extractRedirections( sitemap.tree, { ...data.theme?.value?.redirect } );

	const resolvedHref = computed( () => {
		let url = String( props.href )
			.replace( ptnMarkdownFilename, ".html" )
			.replace( /^!/, "" )            // FIXME clarify what a leading ! is good for
			.replace( ptnAssetPrefix, "" )  // FIXME @assets isn't working in VitePress anymore -> maybe there is a way to replace it properly
			.trim();

		const [ , location = "", tail = "" ] = ptnSplitQueryAndHash.exec( url );

		if ( location !== "" ) {
			url = resolve( route.path, location.replace( ptnIsIndex, "/" ) );

			// look for configured redirection
			url = resolveRedirect( redirections, url );

			if ( ptnIsPage.test( url ) ) {
				const page = sitemap.pathToPage( url, true );

				if ( !page ) {
					console.error( `dead link: reference on locally missing document ${url} referenced as ${props.href}` );
				}
			}

			if ( tail ) {
				url += tail[0];
			}
		}

		return url;
	} );

	const isLocal = computed( () => !ptnHasScheme.test( resolvedHref.value ) );
	const isRelative = computed( () => !ptnIsNonRelative.test( resolvedHref.value ) );
	const isPage = computed( () => ptnIsPage.test( resolvedHref.value ) );
	const isCustomApp = computed( () => !isLocal.value && !ptnHasCommonScheme.test( resolvedHref.value ) );
	const isLocalAnchor = computed( () => ptnIsLocalAnchor.test( resolvedHref.value ) );
	const isNewTab = computed( () => props.external || ( props.external == null && !isLocal.value && !isCustomApp.value ) || props.href.charAt( 0 ) === "!" );
	const isCurrent = computed( () => route.path === resolvedHref.value );
	const isActive = computed( () => route.path?.startsWith( resolvedHref.value ) );

	return {
		resolvedHref,

		isLocal,
		isRelative,
		isCustomApp,
		isPage,
		isLocalAnchor,
		isNewTab,
		isCurrent,
		isActive,

		resolveRelative: url => resolve( resolvedHref.value, url ),
		getRedirection: localPath => resolveRedirect( redirections, localPath ),

		click( event ) {
			if ( resolvedHref.value ) {
				this.$emit( "click", event );

				if ( !event.defaultPrevented ) {
					globalEvent.emit( "reference-clicked", event, this );

					if ( !event.defaultPrevented ) {
						if ( !props.isNewTab && isLocal.value && isPage.value ) {
							if ( this.delay > 0 ) {
								setTimeout( () => {
									router.go( resolvedHref.value );
								}, this.delay );
							} else {
								router.go( resolvedHref.value );
							}

							event.preventDefault();
						} else if ( !isLocal.value ) {
							event.stopPropagation();
						}
					}
				}
			}
		},
	};
}

export const props = {
	href: {
		type: String,
		default: "",
		note: "Optionally selects URL to open on clicking this tile.",
	},
	external: {
		type: Boolean,
		default: undefined,
		note: "Indicates if this reference's target shall be opened in " +
		      "another tab of browser.",
	},
};

/**
 * Replaces provided path name with URL found in given map of redirections.
 *
 * @param {Object<string, (string|function)>} redirections maps a
 * @param {string|URL} currentUrl some URL to look up for probable redirection
 * @returns {string} redirected URL or provided one if no redirection was found
 */
export function resolveRedirect( redirections, currentUrl ) {
	let resolved;

	for ( resolved = String( currentUrl || "" ); resolved && redirections?.hasOwnProperty( resolved ); ) {
		const mapping = redirections[resolved];

		if ( typeof mapping === "function" ) {
			resolved = resolve( resolved, mapping( resolved ) );
		} else {
			resolved = resolve( resolved, mapping );
		}
	}

	return resolved;
}

/**
 * Replaces provided path name with URL found in given map of redirections.
 *
 * @param {Array<VptcSiteTreeNode>} treeOfPages provides tree of pages describing site's overall structure
 * @param {Object<string,string>} resultingMap provides map to be returned for updating and extending
 * @returns {Object<string,string>} maps path names of pages into either page's final redirection target
 */
export function extractRedirections( treeOfPages, resultingMap = {} ) {
	const result = resultingMap ?? {};

	// stage 1: create flat map of all existing redirections
	for ( const node of treeOfPages ) {
		if ( node.frontmatter?.redirect ) {
			const target = node.frontmatter.redirect.replace( /\.md$/, ".html" );
			const source = node.path.replace( /\.md$/, ".html" );

			result[source] = target;
		}

		if ( node.children ) {
			extractRedirections( node.children, result );
		}
	}

	// stage 2: reduce multi-hop redirections
	const resolver = path => {
		if ( result.hasOwnProperty( path ) ) {
			result[path] = resolver( result[path] );
		}

		return path;
	};

	for ( const source of Object.keys( result ) ) {
		result[source] = resolver( result[source] );
	}

	return result;
}
