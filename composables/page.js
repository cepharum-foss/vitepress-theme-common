import { computed, watchEffect } from "vue";
import { useData, useRouter } from "vitepress";
import { useLinkable } from "./linkable.js";
import { ptnIsLocal, resolve } from "../lib/url.js";

/**
 * Exposes API for dealing with a page's metadata usually based on its front
 * matter.
 *
 * @param {object} props properties of a component using this composable
 * @returns {VptcPageComposable} API for processing page's metadata
 */
export function usePage( props ) {
	const router = useRouter();
	const { frontmatter } = useData();
	const { getRedirection } = useLinkable( props );

	const isAmplePage = computed( () => !frontmatter.value.redirect );

	const handleRedirect = () => {
		const { path } = router.route;
		let { redirect } = frontmatter.value || {};

		if ( redirect ) {
			redirect = resolve( path, redirect );

			if ( ptnIsLocal.test( redirect ) ) {
				redirect = getRedirection( redirect.replace( /\.md$/, ".html" ) );
			}

			redirect = resolve( path, redirect );

			if ( redirect !== path ) {
				router.go( redirect );
			}
		}
	};

	watchEffect( handleRedirect );

	return {
		isAmplePage,
	};
}
