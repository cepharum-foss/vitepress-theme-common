const listenersByEventName = {};

/**
 * Exposes methods for interacting with a very basic global event dispatcher.
 *
 * @returns {VptcEventComposable} provides methods for interacting with global event dispatcher
 */
export function useEvent() {
	return {
		emit( name, ...args ) {
			const queue = listenersByEventName[name];

			if ( queue ) {
				for ( let i = 0, length = queue.length; i < length; i++ ) {
					const listener = queue[i];
					const result = listener.fn( ...args );

					if ( listener.once ) {
						queue.splice( i--, 1 );
						length--;
					}

					if ( result === false ) {
						break;
					}
				}
			}
		},
		on( name, fn ) {
			if ( typeof fn === "function" ) {
				listenersByEventName[name] ??= [];
				listenersByEventName[name].push( { fn, once: false } );
			}
		},
		once( name, fn ) {
			if ( typeof fn === "function" ) {
				listenersByEventName[name] ??= [];
				listenersByEventName[name].push( { fn, once: true } );
			}
		},
		off( name, fn ) {
			if ( typeof fn === "function" ) {
				const queue = listenersByEventName[name];

				if ( queue ) {
					for ( let i = 0, length = queue.length; i < length; i++ ) {
						if ( queue[i].fn === fn ) {
							queue.splice( i--, 1 );
							length--;
						}
					}
				}
			}
		},
	};
}
