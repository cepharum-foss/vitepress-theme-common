import { computed } from "vue";
import { useRoute } from "vitepress";

import { asBoolean } from "../lib/utilities.js";

import Sitemap from "../data/sitemap.json";

/**
 * Maps path names of current site's pages into either page's node of site tree.
 *
 * @type {Object<string,VptcSiteTreeNode>}
 */
const pageByPath = {};

/**
 * Detects if provided path name describes an index page or not.
 *
 * @type {RegExp}
 */
export const ptnIsIndex = /\/(?:index\.html)?$/;

/**
 * Caches tree of current site's pages.
 *
 * @type {VptcSiteTreeNode[]}
 */
const tree = createTree( Sitemap, pageByPath );

/**
 * Exposes current site's map of pages and their frontmatter data.
 *
 * @returns {VptcSitemapComposable} exposed properties and methods
 */
export function useSitemap() {
	const route = useRoute();

	const current = computed( () => {
		const node = pageByPath[route.path];
		if ( !node ) {
			return null;
		}

		if ( node.$sparse ) {
			return findAmpleNode( node.children );
		}

		return node;
	} );

	const lookupProperty = ( propertyName, fallback = undefined, forNode = undefined ) => {
		for ( let node = forNode || current.value; node; node = node.$parent ) {
			if ( node.frontmatter.hasOwnProperty( propertyName ) ) {
				return node.frontmatter[propertyName];
			}
		}

		return fallback;
	};

	return {
		tree,

		current,
		closestAmpleParent: computed( () => {
			let longestMatch;

			for ( const candidate of Object.keys( pageByPath ) ) {
				if ( route.path.startsWith( candidate ) && route.path !== candidate ) {
					if ( !longestMatch || longestMatch.length < candidate.length ) {
						longestMatch = candidate;
					}
				}
			}

			if ( !longestMatch ) {
				return null;
			}

			let node;

			for ( node = pageByPath[longestMatch]; node?.$sparse; node = node.$parent ) {
				const sub = findAmpleNode( sub.children, lookupProperty( "sortBy", "sort", node ) );

				if ( sub ) {
					// found the nearest ample page in a shared sparse container
					return sub;
				}
			}

			// found a non-sparse container page
			return node || null;
		} ),

		lookupProperty,
		pageToPath( page, sortBy ) {
			if ( page.$sparse ) {
				return findAmpleNode( page.children, sortBy || lookupProperty( "sortBy", "sort", page ) )?.path;
			}

			return page.path;
		},

		pathToPage( path, ample = false ) {
			const node = pageByPath[path];

			if ( node?.$sparse && ample ) {
				return findAmpleNode( node.children, lookupProperty( "sortBy", "sort", node ) );
			}

			return node || null;
		},
	};
}

/**
 * Converts list of site's pages into multi-level hierarchy.
 *
 * @param {Array<VptcRawSiteTreeNode>} pages lists site's pages and their metadata
 * @param {Object<string,VptcSiteTreeNode>} index maps path names into nodes of created tree
 * @returns {Array<VptcSiteTreeNode>} root level of resulting hierarchy
 */
export function createTree( pages, index = undefined ) {
	const sorted = qualifyPages( pages );
	const asTree = [];

	if ( sorted.length > 0 ) {
		if ( convertListToThread( sorted, null, asTree, 0, index ) < sorted.length ) {
			throw new Error( "failed to put all pages into hierarchy" );
		}
	}

	return asTree;
}

/**
 * Qualifies pages by sorting them by path name and inserting sparse pages for
 * intermediate pages that are missing in filesystem.
 *
 * @param {VptcRawSiteTreeNode[]} pages list of pages to qualify
 * @returns {VptcRawSiteTreeNode[]} qualified list of pages
 */
export function qualifyPages( pages ) {
	// sort pages by path
	const sorted = pages.slice().sort( sortPagesByPath );

	// drop any explicit reference to any folder's index.html
	for ( const page of sorted ) {
		page.path = page.path.replace( ptnIsIndex, "/" );
	}

	// inject sparse page's node for deep hierarchies without intermediate index
	// e.g. if node /section/child/grandchild/sub/test.html is following /section/major.html
	let numItems = sorted.length;

	if ( numItems > 0 && ptnIsIndex.exec( sorted[0].path )?.index !== 0 ) {
		numItems++;
		sorted.unshift( {
			path: "/",
			$sparse: true,
		} );
	}

	let previous = sorted[0]?.path?.replace( /\/$/, "" );

	for ( let i = 1; i < numItems; i++ ) {
		const current = sorted[i].path.replace( /\/$/, "" );

		while ( previous && !current.startsWith( previous ) ) {
			previous = previous.replace( /\/[^/]*$/, "" );
		}

		if ( current.startsWith( previous ) ) {
			const sub = current.substring( previous.length );

			if ( sub[0] === "/" ) {
				const segments = sub.substring( 1 ).replace( /\/+$/, "" ).split( "/" );
				const numSegments = segments.length;

				for ( let j = 1; j < numSegments; j++ ) {
					sorted.splice( i, 0, {
						path: previous + "/" + segments.slice( 0, j ).join( "/" ) + "/",
						$sparse: true,
					} );

					numItems++;
					i++;
				}
			}
		}

		previous = current;
	}

	return sorted;
}

/**
 * Compares to objects by their `path` property.
 *
 * @param {object} left left operand in comparison
 * @param {object} right right operand in comparison
 * @returns {number} -1 if left.path<right.path, 1 if left.path>right.path, 0 if both pathes are nullish
 * @throws if both pathes are set and equal
 */
export function sortPagesByPath( left, right ) {
	if ( left.path ) {
		if ( right.path ) {
			const diff = left.path.localeCompare( right.path );

			if ( !diff ) {
				throw new Error( `invalid duplicate path "${left.path}"` );
			}

			return diff;
		}

		return -1;
	}

	if ( right.path ) {
		return 1;
	}

	return 0;
}

/**
 * Converts slice of listed nodes into hierarchy of nodes.
 *
 * @note This method depends on properly sorted and qualified list of nodes.
 *
 * @param {Array<VptcRawSiteTreeNode>} items lists nodes to be converted
 * @param {VptcSiteTreeNode} parent reference on node to be parent of all converted nodes
 * @param {Array<VptcSiteTreeNode>} level collector for nodes of level in resulting hierarchy
 * @param {int} currentIndex index into list of nodes selecting node to convert next
 * @param {Object<string,VptcSiteTreeNode>} byPath maps path names into nodes of created thread
 * @returns {int} index of item succeeding last processed one
 */
function convertListToThread( items, parent, level, currentIndex, byPath = undefined ) {
	const copy = { frontmatter: {}, ...items[currentIndex] };

	Object.defineProperties( copy, {
		$parent: { value: parent, enumerable: false },
		$level: { value: level, enumerable: false },
		$depth: { value: parent ? parent.$depth + 1 : 0, enumerable: false },
		children: { value: [], enumerable: true },
	} );

	const copyMeta = copy.frontmatter;
	if ( !copyMeta.lang ) {
		const lang = copyMeta.locale || ( parent && parent.frontmatter.lang ) || undefined;

		if ( lang != null ) {
			copyMeta.lang = lang;
		}
	}

	level.push( copy );

	if ( byPath ) {
		byPath[copy.path] = copy; // eslint-disable-line no-param-reassign

		if ( copy.actual ) {
			byPath[copy.actual] = copy; // eslint-disable-line no-param-reassign
		}
	}

	const prefix = copy.path.replace( /\/+$/, "" ) + "/";

	for ( let index = currentIndex + 1; index < items.length; ) {
		const { path } = items[index];

		if ( !path.startsWith( prefix ) ) {
			// ascend for next item isn't subordinated to current one anymore
			return index;
		}

		const moveTo = convertListToThread( items, copy, copy.children, index, byPath );
		if ( moveTo <= index ) {
			throw new Error( "iteration didn't advance as expected" );
		}

		index = moveTo;
	}

	return items.length;
}

/**
 * Finds first ample (by means of non-sparse) node.
 *
 * The function descends into some existing hierarchy of nodes.
 *
 * @param {Array<VptcSiteTreeNode>} level level of nodes to search
 * @param {string} sortBy name of frontmatter property to look up per ample node for sorting
 * @returns {?VptcSiteTreeNode} found ample node, null when all nodes are sparse
 */
function findAmpleNode( level, sortBy ) {
	const numItems = level.length;
	const ampleNodes = [];
	let numeric = true;

	for ( let i = 0; i < numItems; i++ ) {
		const node = level[i];
		const sortIndex = node.frontmatter?.[sortBy];

		if ( sortIndex != null ) {
			if ( /^\s*[+-]?\d+(?:\.\d+)?/.test( sortIndex ) ) {
				numeric &= true;
			} else {
				numeric = false;
			}
		}

		if ( node.$sparse ) {
			const sub = findAmpleNode( node.children, sortBy );
			if ( sub ) {
				ampleNodes.push( { node: sub, index: sortIndex } );
			}
		} else {
			ampleNodes.push( { node, index: sortIndex } );
		}
	}

	if ( numeric ) {
		ampleNodes.sort( ( l, r ) => {
			if ( l.index == null ) {
				if ( r.index == null ) {
					return 0;
				}

				return 1;
			}

			if ( r.index == null ) {
				return -1;
			}

			return parseFloat( l.index, 10 ) - parseFloat( r.index, 10 );
		} );
	} else {
		ampleNodes.sort( ( l, r ) => {
			if ( l.index == null ) {
				if ( r.index == null ) {
					return 0;
				}

				return 1;
			}

			if ( r.index == null ) {
				return -1;
			}

			return String( l.index ).localeCompare( r.index );
		} );
	}

	return ampleNodes[0] || null;
}

/**
 * Collects roots of multiple threads of site hierarchy immediately or mediately
 * subordinated to provided node.
 *
 * @param {Array<VptcSiteTreeNode>} collector array populated with found roots
 * @param {VptcSiteTreeNode} current current node commonly superordinated to all resulting nodes
 * @param {NavigationTreeRange} range minimum and maximum level into site's hierarchy of root nodes to find
 * @param {NavigationTreeLevelSorter} sorter callback invoked for sorting siblings of single level
 * @returns {void}
 */
export function findSubordinatedRoots( collector, current, range, sorter ) {
	if ( current && current.$depth < range.start && current.children.length > 0 ) {
		const level = current.children.slice();

		sortLevel( level, sorter, false );

		if ( current.$depth + 1 === range.start ) {
			collector.splice( collector.length, 0, ...level );
		} else {
			const numItems = level.length;

			for ( let i = 0; i < numItems; i++ ) {
				findSubordinatedRoots( collector, level[i], range, sorter );
			}
		}
	}
}

/**
 * Sorts levels of tree according to selected property in either node's
 * front matter.
 *
 * @param {Array<VptcSiteTreeNode>} level list of nodes in tree (each probably referring to list of subordinated nodes)
 * @param {function(VptcSiteTreeNode, VptcSiteTreeNode):int} sorter callback invoked for comparing two nodes on sorting level
 * @param {boolean} deep controls whether sort subordinated levels as well (true)
 * @returns {Array<VptcSiteTreeNode>} provided list of nodes
 */
export function sortLevel( level, sorter, deep = true ) {
	level.sort( sorter );

	if ( deep ) {
		const numItems = level.length;

		for ( let i = 0; i < numItems; i++ ) {
			const { children } = level[i];

			if ( children.length > 0 ) {
				sortLevel( children, sorter );
			}
		}
	}

	return level;
}

/**
 * Sorts provided level of hierarchy and returns a list of link
 * descriptors.
 *
 * @param {VptcSiteTreeNode[]} level single level of nodes in hierarchy
 * @param {{start: int, stop: int}} range limits range of hierarchy levels to process
 * @param {string} currentPath URL path of current page
 * @param {string} sortBy name of frontmatter property used for sorting either level of hierarchy
 * @param {string} name name of navigation component resulting links will be used with (@see name of <Navigator>)
 * @param {int} slicedDepth indicates depth of level in resulting slice of tree
 * @returns {NavigationLink[]} related hierarchy of link descriptors
 */
export function extractNavigations( level, range, currentPath, sortBy, name, slicedDepth = 0 ) {
	if ( !level || !level.length || level[0].$depth > range.stop ) {
		return [];
	}

	const numItems = level.length;
	const links = new Array( numItems );
	let write = 0;

	for ( let read = 0; read < numItems; read++ ) {
		let node = level[read];

		if ( node.$sparse ) {
			// sparse node adopts content of first ample descendant
			// or gets ignored if missing any ample descendant
			node = findAmpleNode( node.children, sortBy );
			if ( !node ) {
				continue;
			}
		}

		let hidden = false;

		const meta = node.frontmatter || {};
		const props = Object.keys( meta );
		const numProps = props.length;

		for ( let j = 0; j < numProps; j++ ) {
			const prop = props[j];

			switch ( prop.trim().toLowerCase() ) {
				case "demote" :
				case "hide" : {
					let hide = asBoolean( meta[prop], true );

					if ( typeof hide === "string" ) {
						const parts = hide.trim().split( /[\s;,]+/ );
						const numParts = parts.length;
						hide = false;

						for ( let k = 0; k < numParts; k++ ) {
							if ( parts[k] === name ) {
								hide = true;
								break;
							}
						}
					}

					if ( hide ) {
						hidden = true;
					}

					// process first matching frontmatter property, only
					j = Infinity;
				}
			}
		}

		if ( hidden ) {
			continue;
		}

		const label = meta.label || node.title;
		const url = node.path;
		const children = node.children;
		const $depth = node.$depth;

		const selected = url === currentPath || url === `${currentPath}/`;
		const active = selected || ( currentPath.startsWith( url ) && /^(\/|$)/.test( currentPath.substr( url.length ) ) > -1 );

		const sub = children.length ? extractNavigations( children, range, currentPath, sortBy, slicedDepth + 1 ) : [];
		if ( sub.length || !node.$sparse ) {
			const link = {
				url, label, active, selected, $depth, depth: slicedDepth + 1,
			};

			link.children = active || !range.optional ? sub : [];

			links[write++] = link;
		}
	}

	links.splice( write );

	return links;
}
