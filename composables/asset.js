import { computed, ref, watchEffect } from "vue";
import { useUrl } from "./url.js";

import { isServer } from "../lib/utilities.js";
import { useFetch } from "./fetch.js";

/**
 * Generates asset handler.
 *
 * @param {string} propertyName name of property to process
 * @param {object} props properties of component to process
 * @param {object} context application, slots, attributes etc.
 * @returns {VptcAssetComposable} properties and methods for handling an asset
 */
export function useAsset( propertyName = "url", props, context ) {
	const { fetch } = useFetch();
	const url = useUrl( propertyName, props, context );

	const loader = ref( new Promise( () => {} ) );  // eslint-disable-line no-empty-function
	const loading = ref( false );
	const src = ref( null );
	const error = ref( null );

	const document = computed( () => url.isSvg.value && src.value );

	const ready = computed( () => !loading.value && src.value );

	const load = () => {
		error.value = null;

		if ( url.isSvg.value ) {
			src.value = null;
			loading.value = true;

			loader.value = fetch( url.normalized.value )
				.then( response => {
					if ( response.status !== 200 ) {
						throw new Error( `fetching SVG content failed on ${response.status}` );
					}

					const xml = response.content;

					if ( !xml?.rootElement ) {
						throw new Error( `returned content does not offer XML document's API` );
					}

					if ( isServer ) {
						// present a placeholder, only
						src.value = `<?xml version="1.0"?><svg/>`;
					} else {
						// eslint-disable-next-line no-undef
						src.value = new XMLSerializer().serializeToString( xml )
							.replace( /^\s*<\?xml[\s\S]+?\?>/, "" );
					}
				} )
				.catch( cause => {
					error.value = `fetching SVG content of ${props.url} as ${url.normalized.value} failed: ${cause.message}`;
					console.error( error.value );
				} )
				.finally( () => {
					loading.value = false;
				} );
		} else {
			loader.value = Promise.resolve( url.normalized.value );
			src.value = url.normalized.value;
		}
	};

	watchEffect( load );

	return {
		...url,
		loader,
		loading,
		error,
		document,
		src: computed( () => ready.value && src.value ),
		ready,
	};
}
