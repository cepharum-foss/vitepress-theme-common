import { computed } from "vue";

/**
 * Generates methods for dealing with a URL to be found in a named property.
 *
 * @param {string} propName name of property to look for
 * @param {Object<string,any>} props properties of component methods will be used with
 * @returns {VptcUrlComposable} properties and methods for inspecting kind of and interacting with URL found in named property
 */
export function useUrl( propName = "src", props ) {
	const normalized = computed( () => props[propName] );
	const isRemote = computed( () => /^[a-z]+:\/\//i.test( normalized.value ) );
	const isLocal = computed( () => !isRemote.value );
	const isRelative = computed( () => /^\.\.?\//i.test( normalized.value ) );
	const pathname = computed( () => normalized.value.replace( /\?.*$/, "" ) );
	const extension = computed( () => pathname.value.replace( /^.+(\.[^.]+)$/, "$1" ).toLowerCase() );
	const isSvg = computed( () => extension.value === ".svg" );
	const isPng = computed( () => extension.value === ".png" );
	const isJpeg = computed( () => extension.value === ".jpg" || extension.value === ".jpeg" );
	const isGif = computed( () => extension.value === ".gif" );
	const isWebm = computed( () => extension.value === ".webm" );
	const isRasterImage = computed( () => isPng.value || isJpeg.value || isGif.value || isWebm.value );
	const isVectorImage = computed( () => isSvg.value );
	const isImage = computed( () => isRasterImage.value || isVectorImage.value );

	return {
		normalized,
		isRemote,
		isLocal,
		isRelative,
		pathname,
		extension,
		isSvg,
		isPng,
		isJpeg,
		isGif,
		isWebm,
		isRasterImage,
		isVectorImage,
		isImage,
	};
}
