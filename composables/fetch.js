/* eslint-env browser */

import Path from "node:path";
import File from "node:fs";
import { useData } from "vitepress";

import { isServer } from "../lib/utilities.js";

/**
 * Provides composable for commonly fetching resources.
 *
 * @returns {VptcFetchComposable} API for fetching resources
 */
export function useFetch() {
	const data = useData();

	return {
		/**
		 * Fetches resource from remote service.
		 *
		 * @param {URL|string} url URL of resource to fetch
		 * @param {string} method HTTP request method to use
		 * @param {object} query query parameters to be appended to provided URL
		 * @param {Blob|string|object} body content of request body
		 * @param {object} headers custom request headers
		 * @returns {Promise<{status, headers, content}>} promises fetched resource's content combined with response's status code and headers
		 */
		async fetch( url, { method = "GET", query = null, headers = null, body = null } = {} ) {
			// qualify all provided data
			const qualifiedUrl = qualifyUrl( url, query, data.site.value.base, data.page.value.relativePath );
			const qualifiedHeaders = { ...headers };
			const qualifiedBody = typeof body === "object" && body ? JSON.stringify( body ) : body;

			if ( qualifiedBody != null ) {
				let desiredContentType;

				if ( typeof body === "object" && body ) {
					desiredContentType = "application/json";
				} else {
					desiredContentType = typeof body === "string" ? "text/plain" : "application/octet-stream";
				}

				if ( desiredContentType && !Object.keys( qualifiedHeaders ).map( i => i.toLowerCase() ).includes( "content-type" ) ) {
					qualifiedHeaders["content-type"] = desiredContentType;
				}
			}

			if ( qualifiedUrl.protocol === "vptc:" ) {
				// local request on server side
				// ... mock accessing files in site's public/ folder
				// FIXME configuration is collected in a rather hacky way here ... is there a better/more reliable approach?
				const config = await import( "../data/config.json" );

				if ( config?.publicDir === false || !config?.root ) {
					throw new Error( `missing public folder's path name to fetch ${url} on server-side` );
				}

				const result = {
					status: 200,
					headers: new Map(),
				};

				const relativePathname = qualifiedUrl.pathname.slice( data.site.value.base.length || 0 );
				const pathname = Path.resolve( config.root, "public", relativePathname );

				try {
					result.content = await File.promises.readFile( pathname );

					switch ( /\.[^.]+$/.exec( url )?.[0] ) {
						case ".json" :
							result.headers.set( "content-type", "application/json" );
							result.content = JSON.parse( result.content.toString( "utf8" ) );
							break;
					}
				} catch ( cause ) {
					result.status = 404;
				}

				return result;
			}

			if ( isServer ) {
				// remote request on server side
				let httpClient;

				if ( qualifiedUrl.protocol === "https:" ) {
					httpClient = ( await import( "node:https" ) ).request;
				} else {
					httpClient = ( await import( "node:http" ) ).request;
				}

				const response = await new Promise( ( resolve, reject ) => {
					const request = httpClient( qualifiedUrl, {
						method,
						headers: qualifiedHeaders,
					} );

					request.once( "error", reject );
					request.once( "response", resolve );
				} );

				return {
					status: response.statusCode,
					headers: new Map( Object.entries( response.headers ) ),
					content: await receivePayload( response.headers["content-type"], response ),
				};
			}

			// any request of client side
			const request = new Request( qualifiedUrl, {
				method: method || "GET",
				headers: qualifiedHeaders,
				body: qualifiedBody,
			} );

			const response = await window.fetch( request );

			return {
				status: response.status,
				headers: response.headers,
				content: await receivePayload( response.headers.get( "content-type" ), response ),
			};
		}
	};
}

/**
 * Fetches response data as Buffer on server-side.
 *
 * @param {IncomingMessage} message response to some HTTP request
 * @returns {Promise<Buffer>} octets of response payload
 */
function fetchBlob( message ) {
	return new Promise( ( resolve, reject ) => {
		const chunks = [];

		message.once( "error", reject );
		message.on( "data", chunk => { chunks.push( chunk ); } );
		message.once( "end", () => resolve( Buffer.concat( chunks ) ) );
	} );
}

/**
 * Fetches response payload.
 *
 * @param {string} contentType content type reported in response headers
 * @param {Response|IncomingMessage} response response descriptor
 * @returns {Promise<(string|Document|object)>} promises payload of response
 */
function receivePayload( contentType, response ) {
	const mime = contentType.replace( /;.*$/, "" );

	switch ( mime ) {
		case "application/json" :
		case "text/json" :
			if ( isServer ) {
				return fetchBlob( response ).then( buffer => JSON.parse( buffer.toString( "utf8" ) ) );
			}

			return response.json();

		case "text/plain" :
		case "text/css" :
		case "text/html" :
			if ( isServer ) {
				return fetchBlob( response ).then( buffer => buffer.toString( "utf8" ) );
			}

			return response.text();

		case "image/svg+xml" :
		case "text/xml" :
		case "application/xml" :
			if ( isServer ) {
				throw new Error( "server-side fetching does not support delivering XML documents" );
			}

			return response.text().then( xml => new DOMParser().parseFromString( xml, mime ) );

		default :
			if ( isServer ) {
				return fetchBlob( response )
					.then( buffer => new Blob( [buffer.buffer], {
						type: contentType,
					} ) );
			}

			return response.blob();
	}
}

/**
 * Qualifies some caller-provided URL.
 *
 * @param {string|URL} url URL to be qualified
 * @param {Object<string,any>} query optional query parameters to inject into URL
 * @param {string} base base URL of site, used for qualifying relative URLs
 * @param {string} relativePath relative path of context a URL is relative to
 * @returns {URL} qualified URL
 */
function qualifyUrl( url, query, base, relativePath ) {
	let _url;

	if ( typeof url === "string" ) {
		_url = url.startsWith( "/" ) ? `${base}${url.slice( 1 )}` : url;
	} else {
		_url = url;
	}

	const qualified = new URL( _url, isServer ? `vptc://${base}${relativePath}` : location.href );

	for ( const name of Object.keys( query || {} ) ) {
		qualified.searchParams.set( name, query[name] );
	}

	return qualified;
}
