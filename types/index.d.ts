import {ComputedRef} from "vue";

/**
 * Describes interface of very basic API for dispatching and responding to
 * global events.
 */
export interface VptcEventComposable {
	/**
	 * Registers callback for being invoked on emitting named event.
	 *
	 * @param name name of event
	 * @param fn callback to invoke if named event is emitted
	 */
	on( name: string, fn: ( ...args: any ) => boolean ): VptcEventComposable;

	/**
	 * Registers callback for being invoked on emitting named event.
	 *
	 * In addition, the registration is automatically dropped after dispatching
	 * named event once.
	 *
	 * @param name name of event
	 * @param fn callback to invoke if named event is emitted
	 */
	once( name: string, fn: ( ...args: any ) => boolean ): VptcEventComposable;

	/**
	 * Drops registration of provided callback for named event.
	 *
	 * @param name name of event
	 * @param fn callback to invoke if named event is emitted
	 */
	off( name: string, fn: ( ...args: any ) => boolean ): VptcEventComposable;

	/**
	 * Emits named event with optionally provided arguments.
	 *
	 * @param name name of event to emit
	 * @param args optional arguments
	 */
	emit( name: string, ...args: any ): VptcEventComposable;
}

export interface VptcUrlComposable {

}

export interface VptcAssetComposable extends VptcUrlComposable {
	/**
	 * Promise asset being "loaded" or pre-processed.
	 */
	loader: Promise<void>;

	/**
	 * Indicates if asset is currently being "loaded" or not.
	 */
	loading: boolean;

	/**
	 * Provides message of error occurred on trying to load addressed asset.
	 */
	error?: string;

	/**
	 * Provides content of addressed asset if it has been loaded.
	 */
	document?: string;

	/**
	 * Provides URL of addressed asset unless it has been loaded.
	 */
	src?: string;

	/**
	 * If true, asset is ready for injection in DOM.
	 */
	ready: Ref<boolean>;
}

/**
 * Describes API for fetching remote assets.
 */
export interface VptcFetchComposable {
	/**
	 * Fetches remote asset.
	 *
	 * @param url URL of remote asset
	 * @param options options for customizing request made to fetch the asset
	 * @returns result of fetching remote asset
	 */
	fetch(url: URL|string, options: VptcFetchOptions): Promise<VptcFetchResult>;
}

/**
 * Customizes request for fetching remote asset.
 */
export interface VptcFetchOptions {
	/**
	 * Selects HTTP method to use for fetching remote asset. Defaults to GET.
	 */
	method?: string;

	/**
	 * Provides custom parameters to inject into request URL's query.
	 */
	query?: { [name: string]: any };

	/**
	 * Provides custom request headers to use on requesting remote asset.
	 */
	headers?: { [name: string]: any };

	/**
	 * Provides request payload to sent on requesting remote asset.
	 */
	blob?: Blob|string|object;
}

/**
 * Describes result of requesting remote asset.
 */
export interface VptcFetchResult {
	/**
	 * Provides HTTP status code returned by server.
	 */
	status: number;

	/**
	 * Lists response headers.
	 */
	headers: { [name: string]: any };

	/**
	 * Provides response payload which is the requested remote asset.
	 */
	content: Blob|string|Document|object;
}

/**
 * Describes API for interacting with a page's meta data mostly based on its
 * front matter.
 */
export interface VptcPageComposable {
	/**
	 * Indicates if current page is an ample one thus meant to present its own
	 * content.
	 *
	 * If false, the current page is considered to redirect the user to a
	 * different page.
	 */
	isAmplePage: ComputedRef<boolean>;
}

/**
 * Describes composable properties for accessing sitemap which is a tree of
 * current site's pages.
 */
export interface VptcSitemapComposable {
	/**
	 * Exposes whole tree of current site's pages.
	 */
	tree: VptcSiteTreeNode[];

	/**
	 * Refers to current page's node in site's tree of nodes. It is nullish if
	 * current page does not exist.
	 */
	current: VptcSiteTreeNode | null;

	/**
	 * Refers to the nearest ample page super-ordinated to current page. This
	 * works in context of non-existent pages, too.
	 */
	closestAmpleParent: VptcSiteTreeNode | null;

	/**
	 * Reads named property from frontmatter of current page or its nearest
	 * container node.
	 *
	 * @param propertyName name of shallow frontmatter property to look up and read
	 * @param fallback value to return if looking property fails
	 * @param forNode node to start at instead of current page's node
	 * @returns found property's value or nullish, if no property was found
	 */
	lookupProperty( propertyName: string, fallback?: any, forNode?: VptcSiteTreeNode ): any | null;

	/**
	 * Maps provided page into pathname for addressing it or - in case of a
	 * sparse page - its nearest ample ancestor in tree of site's pages.
	 *
	 * @param page node of page in site's tree of pages
	 * @param sortBy name of frontmatter property per page to use for sorting sibling pages
	 * @returns pathname to use for addressing given page
	 */
	pageToPath( page: VptcSiteTreeNode, sortBy?: string ): ?string;

	/**
	 * Picks node of current site's tree based on provided pathname. Resulting
	 *
	 * @param path pathname of page to select
	 * @param ample set true to force selection of ample page
	 * @returns (ample) page associated with provided pathname
	 */
	pathToPage( path: string, ample?: boolean ): ?VptcSiteTreeNode;
}

/**
 * Describes single page in site's tree of pages.
 */
export interface VptcSiteTreeNode {
	/**
	 * Refers to node of page which is super-ordinated to current one.
	 */
	$parent: VptcSiteTreeNode | null;

	/**
	 * Lists current page and its siblings in site's tree of pages.
	 */
	$level: Array<VptcSiteTreeNode>;

	/**
	 * Provides depth of current node in hierarchy/tree of site's nodes.
	 */
	$depth: number;

	/**
	 * If true, this node is part of site tree without an actual page.
	 */
	$sparse?: boolean;

	/**
	 * Lists nodes subordinated to current one in site's tree of nodes.
	 */
	children: Array<VptcSiteTreeNode>;

	/**
	 * Provides path name of page represented by current node.
	 */
	path: string;

	/**
	 * Exposes normalized and qualified meta data of current node.
	 */
	frontmatter: VptcSiteTreeNodeMeta;
}

/**
 * Defines qualified and normalized meta data provided per node of site.
 */
export interface VptcSiteTreeNodeMeta extends VptcRawSiteTreeNodeMeta {
	/**
	 * Provides locale explicitly selected for current node.
	 */
	locale?: string;

	/**
	 * Provides locale of current node inherited from parent node if none is
	 * given on current node explicitly.
	 */
	lang?: string;
}

/**
 * Describes data read from a node's content source file as its YAML-encoded
 * frontmatter.
 */
export interface VptcRawSiteTreeNodeMeta {
	[key: string]: any;
}

/**
 * Defines data collected per document each describing a page of resulting site.
 */
export interface VptcRawSiteTreeNode {
	/**
	 * Provides number of seconds since Unix Epoch of the latest modification of
	 * node's content source.
	 */
	lastUpdated: number;

	/**
	 * Provides optimized path name of page which is unique in context of site.
	 */
	path: string;

	/**
	 * Provides original path name of page which is unique in context of site.
	 * It is given when different from optimized path, only.
	 */
	actual?: string;

	/**
	 * Exposes frontmatter data of current node extracted from its content
	 * source.
	 */
	frontmatter: VptcRawSiteTreeNodeMeta;

	/**
	 * Provides part of node's content source preceding optionally found
	 * `<!-- More -->` marker.
	 */
	excerpt?: string;

	/**
	 * Exposes title based on first level-1 headline found in node's content
	 * source.
	 */
	title?: string;
}

export interface VptcHierarchicalPoolOfNamedValues<T = any> {
	[key: string]: T | VptcHierarchicalPoolOfNamedValues<T>;
}
