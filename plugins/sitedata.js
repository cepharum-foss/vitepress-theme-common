import FastGlob from "fast-glob";
import Path from "node:path";
import File from "node:fs";
import grayMatter from "gray-matter";
import { optimizeRedirections, ptnMarkdownFilename } from "../lib/url.js";

export default {
	name: "cepharum:vitepress-theme-common-sitedata",
	configResolved: {
		async: true,
		parallel: true,
		async handler( cfg ) {
			console.debug( " - [cepharum/common] gathering site data ..." );
			await buildSitedata( cfg );
		},
	},
};

/**
 * Inspects all pages' source files
 *
 * - to collect their front matter for writing the collection to
 *   data/sitemap.json for client-side use e.g. to build navigation controls
 * - to collect all words in content and creating several files in public/ for
 *   client-side search engine.
 *
 * @param {object} config current Vite configuration
 * @returns {Promise<Array<VptcRawSiteTreeNode>>} promises list of extended data each describing a page of site
 */
async function buildSitedata( config ) {
	const pages = await FastGlob( ["**.md"], {
		cwd: config.root,
		ignore: [ "**/node_modules", ...config.srcExclude || [] ]
	} );

	const refsPerKeyword = {};
	const redirections = {};
	const paths = {};

	// inspect and process every markdown file of site
	const sitemap = await Promise.all( pages.sort().map( async( path, pageIndex ) => {
		const name = Path.resolve( config.root, path );
		const [ content, stat ] = await Promise.all( [
			File.promises.readFile( name, "utf-8" ),
			File.promises.stat( name ),
		] );

		const parsed = grayMatter( content );
		const title = /^\s*#\s*([^\s#])$/.exec( content );
		const actual = "/" + path.replace( /\.md$/, ".html" );
		const reduced = actual.replace( /\/index\.html$/, "/" );

		paths[reduced] = pageIndex;

		// create node in sitemap providing either page's metadata
		const meta = {
			lastUpdated: Math.floor( stat.mtimeMs / 1000 ),
			path: reduced,
			frontmatter: parsed.data,
		};

		if ( actual !== reduced ) {
			meta.actual = actual;
		}

		if ( parsed.excerpt ) {
			meta.excerpt = parsed.excerpt;
		}

		if ( title ) {
			meta.title = title[1];
		}

		// separately collect either page's optional redirection target
		if ( parsed.data?.redirect ) {
			redirections[reduced] = parsed.data?.redirect.replace( ptnMarkdownFilename, ".html$1" );
		}

		// extract keywords for full-text searching
		if ( config.publicDir !== false ) {
			for ( const keyword of extractKeywords( parsed.content, config ) ) {
				const heap = refsPerKeyword[keyword] ??= {};

				heap[pageIndex] = ( heap[pageIndex] || 0 ) + 1;
			}
		}

		return meta;
	} ) );

	// post-process collected data in a second run
	optimizeRedirections( redirections );

	const writes = [
		// commonly write collected data to local files picked up by VPCT code
		File.promises.writeFile(
			new URL( "../data/sitemap.json", import.meta.url ),
			JSON.stringify( sitemap ), "utf-8"
		),

		File.promises.writeFile(
			new URL( "../data/paths.json", import.meta.url ),
			JSON.stringify( paths ), "utf-8"
		),

		File.promises.writeFile(
			new URL( "../data/redirections.json", import.meta.url ),
			JSON.stringify( redirections ), "utf-8"
		),

		File.promises.writeFile(
			new URL( "../data/config.json", import.meta.url ),
			JSON.stringify( {
				root: config.root,
				base: config.base,
				publicDir: config.publicDir ?? "public",
				dataDir: config.vptcPublicDataDir ?? "data",
			} ), "utf-8"
		),
	];

	if ( config.publicDir !== false ) {
		// write data files to public folder to enable client-side full-text searching
		const keywords = Object.keys( refsPerKeyword );

		const folder = Path.resolve(
			config.root,
			config.publicDir || "public",
			config.vptcPublicDataDir || "data",
			"search"
		);

		await File.promises.mkdir( folder, { recursive: true } );

		writes.push(
			File.promises.writeFile(
				Path.resolve( folder, "index.json" ),
				JSON.stringify( { keywords: keywords.join( " " ) } )
			),
			...keywords.map( keyword => File.promises.writeFile(
				Path.resolve( folder, `kw-${keyword}.json` ),
				JSON.stringify( refsPerKeyword[keyword] ),
			) ),
		);
	}

	await Promise.all( writes );

	return sitemap;
}

/**
 * Generates word tokens extracted from provided source string.
 *
 * @param {string} source string containing tokens to extracts
 * @param {object} config runtime configuration
 * @returns {Generator<string, void, *>} generator providing token after token extracted from source
 */
function* extractKeywords( source, config ) {
	const pattern = /\p{L}[\p{L}\p{Pc}\p{N}]*/gu;
	let match;

	const noHtml = source.replace( /<[^>]+>/g, "" );
	const minLength = config.searchTerm?.minLength || 3;

	while ( ( match = pattern.exec( noHtml ) ) != null ) {
		const token = match[0].toLocaleLowerCase();

		if ( token.length >= minLength ) {
			yield token;
		}
	}
}
