import Path from "node:path";
import File from "node:fs";

export default {
	name: "cepharum:vitepress-theme-common-l10n-compiler",
	configResolved: {
		async: true,
		parallel: true,
		async handler( cfg ) {
			console.debug( " - [cepharum/common] compiling l10n maps ..." );
			await compileL10nMaps( cfg );
		},
	},
};

/**
 * Compiles l10n maps and exposes them as JSON files in site's public folder.
 *
 * @param {object} config current Vite configuration
 * @returns {Promise<void>} promises l10n maps compiled and written to site's public folder
 */
async function compileL10nMaps( config ) {
	if ( config.publicDir === false ) {
		return;
	}

	const locales = {
		de: ( await import( "../l10n/de.json" ) ).default,
		en: ( await import( "../l10n/en.json" ) ).default,
	};

	let entries = [];

	try {
		entries = await File.promises.readdir( Path.resolve( config.root, ".vitepress/l10n" ) );
	} catch ( cause ) {
		switch ( cause.code ) {
			case "ENOENT" :
			case "ENOTDIR" :
				break;

			default :
				throw cause;
		}
	}

	/**
	 * Process maps for more generic locales, first.
	 */
	await Promise.all( entries.map( async entry => {
		const [ , locale ] = /^([a-z]+)\.json$/i.exec( entry ) || [];

		if ( locale ) {
			const map = await import( Path.resolve( config.root, `.vitepress/l10n/${entry}` ) );

			locales[locale.toLowerCase()] = merge( locales[locale] || {}, map );
		}
	} ) );

	/**
	 * Process maps for more specific locales, second.
	 */
	await Promise.all( entries.map( async entry => {
		const [ , locale, base ] = /^(([a-z]+)-[a-z]+)\.json$/i.exec( entry ) || [];

		if ( locale ) {
			const map = await import( Path.resolve( config.root, `.vitepress/l10n/${entry}` ) );

			locales[locale.toLowerCase()] = merge( {}, locales[base.toLowerCase()] );
			locales[locale.toLowerCase()] = merge( locales[locale] || {}, map );
		}
	} ) );

	/**
	 * Writes all compiles maps to JSON files.
	 */
	const folder = Path.resolve(
		config.root,
		config.publicDir || "public",
		config.vptcPublicDataDir || "data",
		"l10n"
	);

	await File.promises.mkdir( folder, { recursive: true } );

	await Promise.all( Object.keys( locales ).map( async locale => {
		await File.promises.writeFile(
			Path.resolve( folder, `${locale}.json` ),
			JSON.stringify( locales[locale] )
		);
	} ) );
}

/**
 * Deeply merges to hierarchical maps of translations.
 *
 * @param {object} dst map to be adjusted by merging
 * @param {object} src map to be merged into first provided one
 * @returns {object} first provided map
 */
function merge( dst, src ) {
	if ( dst && src && typeof dst === "object" && typeof src === "object" ) {
		for ( const name of Object.keys( src ) ) {
			switch ( name ) {
				case "constructor" :
				case "prototype" :
				case "__proto__" :
					continue;

				default : {
					const value = src[name];

					if ( typeof value === "string" ) {
						dst[name] = value;                      // eslint-disable-line no-param-reassign
					} else if ( typeof value === "object" && value ) {
						const ref = dst[name] && typeof dst[name] === "object" ? dst[name] : {};

						dst[name] = merge( ref, src[name] );    // eslint-disable-line no-param-reassign
					}
				}
			}
		}
	}

	return dst;
}
