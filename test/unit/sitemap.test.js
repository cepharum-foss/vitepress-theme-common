import { describe, it, expect, expectTypeOf } from "vitest";
import { qualifyPages, createTree } from "../../composables/sitemap.js";

const sparsing = path => ( { path, $sparse: true } );

describe( "Sitemap composable", () => {
	describe( "provides a function qualifyPages() which", () => {
		it( "is a function", () => {
			expect( qualifyPages ).toBeTypeOf( "function" );
		} );

		it( "requires one argument", () => {
			expect( qualifyPages.length ).toBe( 1 );
		} );

		it( "returns empty list of pages as provided", () => {
			expect( qualifyPages( [] ) ).toStrictEqual( [] );
		} );

		it( "returns list with single index page at root level as provided", () => {
			const page = { path: "/index.html" };

			expect( qualifyPages( [page] ) ).toStrictEqual( [page] );
		} );

		it( "inserts another sparse index page before single non-index page in a sub folder", () => {
			const page = { path: "/foo.html" };
			const [rootIndex] = ["/"].map( path => ( { path, $sparse: true } ) );

			expect( qualifyPages( [page] ) ).toStrictEqual( [ rootIndex, page ] );
		} );

		it( "inserts another sparse index page before single index page in a sub folder", () => {
			const page = { path: "/major/index.html" };
			const [rootIndex] = ["/"].map( path => ( { path, $sparse: true } ) );

			expect( qualifyPages( [page] ) ).toStrictEqual( [ rootIndex, page ] );
		} );

		it( "inserts two sparse index pages before single non-index page in a sub folder", () => {
			const page = { path: "/major/bar.html" };
			const [ rootIndex, localIndex ] = [ "/", "/major/" ].map( sparsing );

			expect( qualifyPages( [page] ) ).toStrictEqual( [ rootIndex, localIndex, page ] );
		} );

		it( "inserts sparse index pages before single index page in a deep sub folder", () => {
			const page = { path: "/major/minor/sub/index.html" };
			const [ root, major, minor ] = [ "/", "/major/", "/major/minor/", "/major/minor/" ].map( sparsing );

			expect( qualifyPages( [page] ) ).toStrictEqual( [ root, major, minor, page ] );
		} );

		it( "inserts sparse index pages before single non-index page in a deep sub folder", () => {
			const page = { path: "/major/minor/sub/bar.html" };
			const [ root, major, minor, sub ] = [ "/", "/major/", "/major/minor/", "/major/minor/sub/" ].map( sparsing );

			expect( qualifyPages( [page] ) ).toStrictEqual( [ root, major, minor, sub, page ] );
		} );

		it( "inserts sparse index pages before and between multiple deeply nested non-index pages", () => {
			const deep = { path: "/major/foo.html" };
			const deeper = { path: "/major/minor/sub/bar.html" };
			const [ root, major, minor, sub ] = [ "/", "/major/", "/major/minor/", "/major/minor/sub/" ].map( sparsing );

			expect( qualifyPages( [ deep, deeper ] ) ).toStrictEqual( [ root, major, deep, minor, sub, deeper ] );
			expect( qualifyPages( [ deeper, deep ] ) ).toStrictEqual( [ root, major, deep, minor, sub, deeper ] );
		} );

		it( "keeps nested index pages on inserting sparse pages before and between multiple deeply nested pages", () => {
			const deep = { path: "/major/index.html" };
			const deeper = { path: "/major/minor/" };
			const deepest = { path: "/major/minor/sub/bar.html" };
			const [ root, sub ] = [ "/", "/major/minor/sub/" ].map( sparsing );

			expect( qualifyPages( [ deep, deeper, deepest ] ) ).toStrictEqual( [ root, deep, deeper, sub, deepest ] );
			expect( qualifyPages( [ deepest, deeper, deep ] ) ).toStrictEqual( [ root, deep, deeper, sub, deepest ] );
			expect( qualifyPages( [ deeper, deepest, deep ] ) ).toStrictEqual( [ root, deep, deeper, sub, deepest ] );
			expect( qualifyPages( [ deeper, deep, deepest ] ) ).toStrictEqual( [ root, deep, deeper, sub, deepest ] );
		} );

		it( "rejects multiple pages with same path", () => {
			const deep1 = { path: "/major/index.html" };
			const deep2 = { path: "/major/index.html" };
			const deeper = { path: "/major/minor/sub/bar.html" };

			expect( () => qualifyPages( [ deep1, deep2, deeper ] ) ).toThrowError();
			expect( () => qualifyPages( [ deeper, deep2, deep1 ] ) ).toThrowError();
		} );
	} );

	describe( "provides a function createTree() which", () => {
		it( "is a function", () => {
			expectTypeOf( createTree ).toBeFunction();
		} );

		it( "requires one argument", () => {
			expect( createTree ).toHaveLength( 1 );
		} );

		it( "creates empty list of root nodes on providing empty list of pages", () => {
			expect( createTree( [] ) ).toStrictEqual( [] );
		} );

		it( "lists single root page's thread", () => {
			const tree = createTree( [{ path: "/" }] );

			expectTypeOf( tree ).toBeArray();
			expect( tree ).toHaveLength( 1 );
			expect( tree[0].path ).toStrictEqual( "/" );
			expect( tree[0].$parent ).toBeNull();
			expect( tree[0].$depth ).toBe( 0 );
			expect( tree[0].$level ).toStrictEqual( tree );
			expect( tree[0].$sparse ).toBeFalsy();
			expect( tree[0].frontmatter ).toStrictEqual( {} );
			expect( tree[0].children ).toStrictEqual( [] );
		} );

		it( "creates a two-node thread for a single non-index root-level page", () => {
			const tree = createTree( [{ path: "/foo.html" }] );

			expectTypeOf( tree ).toBeArray();
			expect( tree ).toHaveLength( 1 );
			expect( tree[0].path ).toStrictEqual( "/" );
			expect( tree[0].$parent ).toBeNull();
			expect( tree[0].$depth ).toBe( 0 );
			expect( tree[0].$level ).toStrictEqual( tree );
			expect( tree[0].$sparse ).toBe( true );
			expect( tree[0].frontmatter ).toStrictEqual( {} );
			expect( tree[0].children ).not.toStrictEqual( [] );

			expect( tree[0].children ).toHaveLength( 1 );
			expect( tree[0].children[0].path ).toStrictEqual( "/foo.html" );
			expect( tree[0].children[0].$parent ).toStrictEqual( tree[0] );
			expect( tree[0].children[0].$depth ).toBe( 1 );
			expect( tree[0].children[0].$level ).toStrictEqual( tree[0].children );
			expect( tree[0].children[0].$sparse ).toBeFalsy();
			expect( tree[0].children[0].frontmatter ).toStrictEqual( {} );
			expect( tree[0].children[0].children ).toStrictEqual( [] );
		} );

		it( "keeps original frontmatter of ample pages", () => {
			const frontmatter = {};
			const tree = createTree( [{ path: "/foo.html", frontmatter }] );

			expectTypeOf( tree ).toBeArray();
			expect( tree ).toHaveLength( 1 );
			expect( tree[0].path ).toStrictEqual( "/" );
			expect( tree[0].$parent ).toBeNull();
			expect( tree[0].$depth ).toBe( 0 );
			expect( tree[0].$level ).toStrictEqual( tree );
			expect( tree[0].$sparse ).toBe( true );
			expect( tree[0].frontmatter ).toStrictEqual( {} );
			expect( tree[0].frontmatter ).not.toBe( frontmatter );
			expect( tree[0].children ).not.toStrictEqual( [] );

			expect( tree[0].children ).toHaveLength( 1 );
			expect( tree[0].children[0].path ).toStrictEqual( "/foo.html" );
			expect( tree[0].children[0].$parent ).toStrictEqual( tree[0] );
			expect( tree[0].children[0].$depth ).toBe( 1 );
			expect( tree[0].children[0].$level ).toStrictEqual( tree[0].children );
			expect( tree[0].children[0].$sparse ).toBeFalsy();
			expect( tree[0].children[0].frontmatter ).toStrictEqual( {} );
			expect( tree[0].children[0].frontmatter ).toBe( frontmatter );
			expect( tree[0].children[0].children ).toStrictEqual( [] );
		} );

		it( "creates a three-node thread for a two non-index root-level pages", () => {
			const tree = createTree( [ { path: "/foo.html" }, { path: "/bar.html" } ] );

			expectTypeOf( tree ).toBeArray();
			expect( tree ).toHaveLength( 1 );
			expect( tree[0].path ).toStrictEqual( "/" );
			expect( tree[0].$parent ).toBeNull();
			expect( tree[0].$depth ).toBe( 0 );
			expect( tree[0].$level ).toStrictEqual( tree );
			expect( tree[0].$sparse ).toBe( true );
			expect( tree[0].frontmatter ).toStrictEqual( {} );
			expect( tree[0].frontmatter ).not.toBe( tree[0].children[0].frontmatter );
			expect( tree[0].frontmatter ).not.toBe( tree[0].children[1].frontmatter );
			expect( tree[0].children ).not.toStrictEqual( [] );

			expect( tree[0].children ).toHaveLength( 2 );
			expect( tree[0].children[0].path ).toStrictEqual( "/bar.html" );
			expect( tree[0].children[0].$parent ).toStrictEqual( tree[0] );
			expect( tree[0].children[0].$depth ).toBe( 1 );
			expect( tree[0].children[0].$level ).toStrictEqual( tree[0].children );
			expect( tree[0].children[0].$sparse ).toBeFalsy();
			expect( tree[0].children[0].frontmatter ).toStrictEqual( {} );
			expect( tree[0].children[0].frontmatter ).not.toBe( tree[0].children[1].frontmatter );
			expect( tree[0].children[0].frontmatter ).not.toBe( tree[0].frontmatter );
			expect( tree[0].children[0].children ).toStrictEqual( [] );

			expect( tree[0].children[1].path ).toStrictEqual( "/foo.html" );
			expect( tree[0].children[1].$parent ).toStrictEqual( tree[0] );
			expect( tree[0].children[1].$depth ).toBe( 1 );
			expect( tree[0].children[1].$level ).toStrictEqual( tree[0].children );
			expect( tree[0].children[1].$sparse ).toBeFalsy();
			expect( tree[0].children[1].frontmatter ).toStrictEqual( {} );
			expect( tree[0].children[1].frontmatter ).not.toBe( tree[0].children[0].frontmatter );
			expect( tree[0].children[1].frontmatter ).not.toBe( tree[0].frontmatter );
			expect( tree[0].children[1].children ).toStrictEqual( [] );
		} );
	} );
} );
