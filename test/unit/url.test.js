import { describe, it, expect } from "vitest";
import { resolve } from "../../lib/url";

describe( "URL library", () => {
	describe( "provides function resolve() which", () => {
		it( "is a function", () => {
			expect( resolve ).toBeTypeOf( "function" );
		} );

		it( "requires two arguments", () => {
			expect( resolve.length ).toBe( 2 );
		} );

		it( "returns first argument if second one is falsy", () => {
			expect( resolve( "foo", undefined ) ).toBe( "foo" );
			expect( resolve( "foo", null ) ).toBe( "foo" );
			expect( resolve( "foo", false ) ).toBe( "foo" );
			expect( resolve( "foo", "" ) ).toBe( "foo" );
		} );

		it( "returns second argument if it's starting with a forward slash", () => {
			expect( resolve( "foo", "/bam" ) ).toBe( "/bam" );
			expect( resolve( "/foo/bar/baz", "/bam" ) ).toBe( "/bam" );
			expect( resolve( "/foo/bar/baz", "/" ) ).toBe( "/" );
		} );

		it( "returns second argument if it's starting with a hash", () => {
			expect( resolve( "foo", "#bam" ) ).toBe( "#bam" );
			expect( resolve( "/foo/bar/baz", "#bam" ) ).toBe( "#bam" );
		} );

		it( "returns second argument if it's starting with a question mark", () => {
			expect( resolve( "foo", "?bam=1" ) ).toBe( "?bam=1" );
			expect( resolve( "/foo/bar/baz", "?bam=1" ) ).toBe( "?bam=1" );
		} );

		it( "replaces last segment of first path name with second argument", () => {
			expect( resolve( "foo", "bam" ) ).toBe( "/bam" );
			expect( resolve( "/foo/bar/baz", "bam" ) ).toBe( "/foo/bar/bam" );
		} );

		it( "ignores segments consisting of sole periods in second argument", () => {
			expect( resolve( "foo", "./bam" ) ).toBe( "/bam" );
			expect( resolve( "foo", "./bam/." ) ).toBe( "/bam" );
			expect( resolve( "/foo/bar/baz", "./bam" ) ).toBe( "/foo/bar/bam" );
			expect( resolve( "/foo/bar/baz", "./bam/." ) ).toBe( "/foo/bar/bam" );
		} );

		it( "drops another segment on encountering segments consisting of double period in second argument", () => {
			expect( resolve( "/foo/bar/baz", "../bam" ) ).toBe( "/foo/bam" );
			expect( resolve( "/foo/bar/baz", "../bam/.." ) ).toBe( "/foo" );
		} );

		it( "ignores additional segments consisting of double period in second argument if all segments of first argument have been dropped already", () => {
			expect( resolve( "/foo/bar/baz", "../../../../bam" ) ).toBe( "/bam" );
			expect( resolve( "/foo/bar/baz", "../bam/../../../.." ) ).toBe( "/" );
		} );
	} );
} );
