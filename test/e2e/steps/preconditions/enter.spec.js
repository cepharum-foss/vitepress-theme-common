const { Given } = require( "@cucumber/cucumber" );

Given( "I open browser at {word}", async( t, [pathname] ) => {
	await t
		.navigateTo( `${process.env.SERVER || "http://localhost:4173"}${process.env.BASE || "/custom/base"}${pathname}` );
} );
