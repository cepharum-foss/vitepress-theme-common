const { Given, Then } = require( "@cucumber/cucumber" );
const { ClientFunction } = require( "testcafe" );

const getHistoryLength = ClientFunction( () => history.length ); // eslint-disable-line no-undef

Given( "I remember current length of history", async t => {
	t.ctx.historyLength = await getHistoryLength(); // eslint-disable-line no-param-reassign
} );

Then( "history length has increased by {int}", async( t, [count] ) => {
	await t
		.expect( await getHistoryLength() - t.ctx.historyLength )
		.eql( count, "unexpected growth of browser history size" );
} );

Then( "history has length of {int}", async( t, [count] ) => {
	await t
		.expect( await getHistoryLength() )
		.eql( count, "unexpected size of browser history" );
} );
