const { Before } = require( "@cucumber/cucumber" );
const { ContextualGherkin } = require( "@cepharum/contextual-gherkin" );

Before( t => {
	ContextualGherkin( t, {
		selectors: {
			headline: "h1"
		},
		aliases: [
			[ "headline", "title" ]
		],
	} );
} );
