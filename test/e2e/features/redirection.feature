Feature: Redirection

Scenario: no redirection on entering site via existing page
	Given I remember current length of history
	And I open browser at /en/welcome.html
	Then history length has increased by 1

Scenario: replacing redirection on entering site via redirecting page
	Given I remember current length of history
	And I open browser at /en/index.html
	Then history length has increased by 1

Scenario: replacing redirection on entering site via folder with an index page
	Given I remember current length of history
	And I open browser at /en/
	Then history length has increased by 1

Scenario: replacing redirection on entering site via folder with an index page w/o using tailing slash
	Given I remember current length of history
	And I open browser at /en
	Then history length has increased by 1
