import {createThemeWithLayout} from "../../../../../application.js";
import Layout from "./layout.vue";

export default createThemeWithLayout( Layout );
