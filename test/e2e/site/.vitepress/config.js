import { enhanceConfig } from "../../../../config";

export default enhanceConfig( {
	title: "E2E testing website",
	base: "/custom/base/",
	vite: {
		vptcPublicDataDir: ".data"
	}
} );
