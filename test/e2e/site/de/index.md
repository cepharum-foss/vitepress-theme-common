---
lang: de
redirect: willkommen.md
---

# Leite weiter

Diese Seite sollte nicht sichtbar bleiben, da sie eine Weiterleitung zur eigentlichen Seite deklariert.
